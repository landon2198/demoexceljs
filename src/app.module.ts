import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ExcelTestModule } from './excel-test/excel-test.module';
import { HandleTemplateService } from './handle-template/handle-template.service';
import { HandleTemplateModule } from './handle-template/handle-template.module';

@Module({
  imports: [ExcelTestModule, HandleTemplateModule],
  controllers: [AppController],
  providers: [AppService, HandleTemplateService],
})
export class AppModule {}
