import { Module } from '@nestjs/common';
import { ExcelTestService } from './excel-test.service';
import { ExcelTestController } from './excel-test.controller';

@Module({
  providers: [ExcelTestService],
  controllers: [ExcelTestController]
})
export class ExcelTestModule {}
