import { Controller, Post } from '@nestjs/common';
import { ExcelTestService } from './excel-test.service'

@Controller('excel-test')
export class ExcelTestController {
    constructor(
        private excelTestService : ExcelTestService
    ){}

    @Post('/create')
    createExcel(){
        return this.excelTestService.generateExcel();
    }
}
