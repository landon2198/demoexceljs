import { Injectable } from '@nestjs/common';
import * as ExcelJS from 'exceljs';
// import { merge } from 'rxjs';

@Injectable(
)
export class ExcelTestService {
    constructor() {
    }
    async generateExcel() {
        //*Create a Workbook
        const workbook = new ExcelJS.Workbook();
        // await workbook.xlsx.writeFile('export.xlsx');
        // const readFile = await workbook.xlsx.readFile('export.xlsx');

        //*Set Workbook properties ==================================================================================
        workbook.creator = 'Me';
        workbook.lastModifiedBy = 'Her'
            //*TODO:Nhỏ hơn 1 tháng so với tháng trong Properties của excel
        const date = new Date('1988-03-21');
        console.log('Date: '+ date)

        workbook.created = date;
        workbook.modified = new Date();
            //*TODO:Nhỏ hơn 1 tháng so với tháng trong Properties của excel
        workbook.lastPrinted = new Date(2016, 9, 27);

            // *Set workbook dates to 1904 date system
        workbook.properties.date1904 = true;

        //*Set Calculation Properties ===============================================================================
            //* Force workbook calculation on load
            //*TODO:Không hiểu cách hoạt động 
        workbook.calcProperties.fullCalcOnLoad = true;

        //*Workbook Views ============================================================================================
            //*TODO:Chua hieu ve x,y,width,height
        workbook.views = [
            {
              x: 0, y: 0, width: 10000, height: 10000,
              firstSheet: 0, activeTab: 2, visibility: 'visible'
            }
          ]

        //*Add a Worksheet ==========================================================================================
        const worksheet = workbook.addWorksheet("My Sheet", {properties:{outlineLevelCol:3}});

          //*Use the second parameter of the addWorksheet function to specify options for the worksheet.
        const worksheet1 = workbook.addWorksheet('My Sheet 1', {
            headerFooter:{firstHeader: "Hello Exceljs", firstFooter: "Hello World"}
        });
        const worksheet2 = workbook.addWorksheet('My Sheet 2');

        const worksheet3 = workbook.addWorksheet('My Sheet 3', {properties:{outlineLevelCol:5}});

        const worksheet4 = workbook.addWorksheet('My Sheet 4', {views: [{showGridLines: false}]});

        const worksheet5 = workbook.addWorksheet('My Sheet 5', {views:[{state: 'frozen', xSplit: 1, ySplit:1}]});

        //*TODO: Chua hieu ve paperSize
        const worksheet6=  workbook.addWorksheet('My Sheet 6', {
            pageSetup:{paperSize: 9, orientation:'landscape'}
          });

        const worksheet7 = workbook.addWorksheet("My Sheet 7");

        //*Remove a worksheet =======================================================================================
        workbook.removeWorksheet(worksheet7.id)

        //*Access Worksheets =========================================================================================
        const getWorkSheet = workbook.getWorksheet('My Sheet');
        // const getWorkSheet = workbook.getWorksheet(4);
        // const getWorkSheet = workbook.worksheets[3];
        // console.log(getWorkSheet)
        
        //*Worksheet State ===========================================================================================
        //*TODO: Chua hieu ve state trong excel
        // make worksheet visible
        // worksheet.state = 'visible';

        // make worksheet hidden
        // worksheet.state = 'hidden';

        // make worksheet hidden from 'hide/unhide' dialog
        // worksheet.state = 'veryHidden';

        //*Worksheet Properties =====================================================================================
        // adjust properties afterwards (not supported by worksheet-writer)
        worksheet1.properties.outlineLevelCol = 2;
        worksheet1.properties.defaultRowHeight = 25;

        //*TODO: Chua test duoc
        //*Page Setup ================================================================================================
        worksheet6.pageSetup.margins = {
            left: 0.7, right: 0.7,
            top: 0.75, bottom: 0.75,
            header: 0.3, footer: 0.3
        }
        //* Set Print Area for a sheet
        worksheet6.pageSetup.printArea = 'A1:G20';

        //* Set multiple Print Areas by separating print areas with '&&'
        worksheet6.pageSetup.printArea = 'A1:G10&&A11:G20';

        //* Repeat specific rows on every printed page
        worksheet6.pageSetup.printTitlesRow = '1:3';

        //* Repeat specific columns on every printed page
        worksheet6.pageSetup.printTitlesColumn = 'A:C';

        //*Headers and Footers ======================================================================================
        //* Set footer (default centered), result: "Page 2 of 16"
        worksheet1.headerFooter.oddFooter = "Page &P of &N";

        //* Set the footer (default centered) to bold, resulting in: "Page 2 of 16"
        worksheet1.headerFooter.oddFooter = "Page &P of &N";

        //* Set the left footer to 18px and italicize. Result: "Page 2 of 16"
        worksheet1.headerFooter.oddFooter = "&LPage &P of &N";

        //* Set the middle header to gray Aril, the result: "52 exceljs"
        worksheet1.headerFooter.oddHeader = "&C&KCCCCCC&\"Aril\"52 exceljs";

        //* Set the left, center, and right text of the footer. Result: “Exceljs” in the footer left. “demo.xlsx” in the footer center. “Page 2” in the footer right
        worksheet1.headerFooter.oddFooter = "&Lexceljs&C&F&RPage &A";

        //* Add different header & footer for the first page
        worksheet1.headerFooter.differentFirst = true;
        worksheet1.headerFooter.firstHeader = "Hello Exceljs";
        worksheet1.headerFooter.firstFooter = "Hello World"

        //*Split Views ============================================================================================
        worksheet1.views = [
            {state: 'split', xSplit: 2000, ySplit: 2000, topLeftCell: 'G10', activeCell: 'F1'}
          ];

        //*Auto Filters
        //*TODO: Chua hieu ve chuc nang cua auto filter
        // worksheet.autoFilter = 'A2:B2';
        //* Set an auto filter from the cell in row 3 and column 1
        //* to the cell in row 5 and column 12
        // worksheet.autoFilter = {
        //     from: {
        //     row: 3,
        //     column: 1
        //     },
        //     to: {
        //     row: 5,
        //     column: 12
        //     }
        // }
        
        //* Set an auto filter from D3 to the
        //* cell in row 7 and column 5
        worksheet.autoFilter = {
            from: 'C3',
            to: {
            row: 7,
            column: 5
            }
        }

        //*Columns ==================================================================================================

        worksheet.columns = [
            {header: 'Id', key: 'id', width: 10},
            {header: 'Name', key: 'name', width: 32}, 
            {header: 'D.O.B.', key: 'dob', width: 15,outlineLevel: 1}
        ];

        worksheet.addRow({id: 1, name: 'John Doe', dob: new Date(1970, 1, 1)}, 'i');
        worksheet.addRow({id: 2, name: 'Jane Doe', dob: new Date(1965, 1, 7)}, 'i');
        worksheet.addRow({id: 3, name: 'Jack', dob: new Date('1970, 1, 1').toString()}, 'i');
        worksheet.addRow({id: 4, name: 'Andre', dob: new Date(1965, 1, 7)}, 'i');
        worksheet.addRow({id: 5, name: 'Lucas', dob: new Date(1970, 1, 1)}, 'i');
        worksheet.addRow({id: 6, name: 'Jame', dob: new Date(1965, 1, 7)}, 'i');

        //* Access an individual columns by key, letter and 1-based column number
        const idCol = worksheet.getColumn('id');
        const nameCol = worksheet.getColumn('B');
        const dobCol = worksheet.getColumn(3);
        // console.log(dobCol)

        //* Note: will overwrite cell value C1
        dobCol.header = 'Date of Birth';

        //* Note: this will overwrite cell values C1:C2
        dobCol.header = ['Date of Birth', 'A.K.A. D.O.B.'];

        //* from this point on, this column will be indexed by 'dob' and not 'DOB'
        dobCol.key = 'dob';
        dobCol.width = 15;
        //* Hide the column if you'd like
        // dobCol.hidden = true;

        //* set an outline level for columns
        worksheet.getColumn(1).outlineLevel = 0;
        worksheet.getColumn(2).outlineLevel = 1;
        worksheet.getColumn(3).outlineLevel = 1;
        
        //* columns support a readonly field to indicate the collapsed state based on outlineLevel
        //*TODO: Code loi
        // expect(worksheet.getColumn(2).collapsed).to.equal(false);
        // expect(worksheet.getColumn(3).collapsed).to.equal(true);

        //* iterate over all current cells in this column
        dobCol.eachCell(function(cell, rowNumber) {
            // ...
        });
        
        //* iterate over all current cells in this column including empty cells
        dobCol.eachCell({ includeEmpty: true }, function(cell, rowNumber) {
            // ...
        });

        //* add a column of new values
        worksheet.getColumn(4).values = [1,2,3,4,5,6,7];

        //* add a sparse column of values
        worksheet.getColumn(5).values = [,,2,3,,5,,7,,,,11];

        //* cut one or more columns (columns to the right are shifted left)
        //* If column properties have been defined, they will be cut or moved accordingly
        //* Known Issue: If a splice causes any merged cells to move, the results may be unpredictable
        // worksheet.spliceColumns(2,3);

        //* remove one column and insert two more.
        //* Note: columns 4 and above will be shifted right by 1 column.
        //* Also: If the worksheet has more rows than values in the column inserts,
        //*  the rows will still be shifted as if the values existed
        // const newCol3Values = [1,2,3,4,5];
        // const newCol4Values = ['one', 'two', 'three', 'four', 'five'];
        // worksheet.spliceColumns(3, 1, newCol3Values, newCol4Values);

        //*Rows ====================================================================================================
        //* Get a row object. If it doesn't already exist, a new empty one will be returned
        const row = worksheet.getRow(5);
        const row1 = worksheet.getRow(6);

        //* Get the last editable row in a worksheet (or undefined if there are none)
        const row2 = worksheet.lastRow;
        //* Set a specific row height
        row.height = 42.5;

        //* make row hidden
        // row.hidden = true;

        //* set an outline level for rows
        worksheet.getRow(1).outlineLevel = 0;
        worksheet.getRow(2).outlineLevel = 1;
        worksheet.getRow(3).outlineLevel = 1;
        worksheet.getRow(4).outlineLevel = 1;
        worksheet.getRow(5).outlineLevel = 1;

        //* rows support a readonly field to indicate the collapsed state based on outlineLevel
        //*TODO: Code loi
        // expect(worksheet.getRow(4).collapsed).to.equal(false);
        // expect(worksheet.getRow(5).collapsed).to.equal(true);

        row.getCell(1).value = 4; // A5's value set to 5
        row.getCell('name').value = 'Zeb'; // B5's value set to 'Zeb' - assuming column 2 is still keyed by name
        row.getCell('C').value = new Date(); // C5's value set to now

        //* Get a row as a sparse array
        //* Note: interface change: worksheet.getRow(4) ==> worksheet.getRow(4).values
        //*TODO: Code khong hoat dong
        // const row3 = worksheet.getRow(2).values;
        // expect(row3[1]).toEqual('John Doe');

        //* assign row values by contiguous array (where array element 0 has a value)
        row.values = [1,2,3,4,5,6,7,8,9,0];
        //*TODO: Code khong hoat dong
        // expect(row.getCell(1).value).toEqual(1);
        // expect(row.getCell(2).value).toEqual(2);
        // expect(row.getCell(3).value).toEqual(3);

        //* assign row values by object, using column keys
        row2.values = {
            id: 13,
            name: 'Thing 1',
            dob: new Date()
        };

        //* Insert a page break below the row
        //*TODO: Code khong hoat dong
        row.addPageBreak();

        //* Iterate over all rows that have values in a worksheet
        // worksheet.eachRow(function(row, rowNumber) {
        //     console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values));
        // });

        //* Iterate over all rows (including empty rows) in a worksheet
        // worksheet.eachRow({ includeEmpty: true }, function(row, rowNumber) {
        //     console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values));
        // });

        //* Iterate over all non-null cells in a row
        // row1.eachCell(function(cell, colNumber) {
        //     console.log('Cell ' + colNumber + ' = ' + cell.value);
        // });

        //* Iterate over all cells in a row (including empty cells)
        // row1.eachCell({ includeEmpty: true }, function(cell, colNumber) {
        //     console.log('Cell ' + colNumber + ' = ' + cell.value);
        // });

        //* Commit a completed row to stream
        //*TODO: Khong hieu chuc nang cua row.commit
        row.commit();

        //* row metrics
        const rowSize = row.cellCount;
        const numValues = row.actualCellCount;

        //*Add Rows ==================================================================================================
        //* Add a couple of Rows by key-value, after the last current row, using the column keys
        worksheet2.columns = [
            {header: 'Id', key: 'id', width: 10},
            {header: 'Name', key: 'name', width: 32}, 
            {header: 'D.O.B.', key: 'dob', width: 15,outlineLevel: 1}
        ];
        worksheet2.addRow({id: 1, name: 'John Doe', dob: new Date(1970, 1, 1)}, 'i');
        worksheet2.addRow({id: 2, name: 'Jack', dob: new Date(1970,1,1)},'i');
        worksheet2.addRow({id: 3, name: 'Cris', dob: new Date(1965,1,7)},'i');
        //* Add a row by contiguous Array (assign to columns A, B & C)
        worksheet2.addRow([4, 'Sam', new Date()],'i');

        //* Add a row by sparse Array (assign to columns A, E & I)
        const rowValues = [];
        rowValues[1] = 5;
        rowValues[5] = 'Kyle';
        rowValues[9] = new Date();
        worksheet2.addRow(rowValues,'i');

        //* Add an array of rows
        const rows = [
            [6,'Bob',new Date()], // row by array
            {id:7, name: 'Barbara', dob: new Date()}
        ];
        worksheet2.addRows(rows,'n');

        //*Handling Individual Cells ===================================================================================
        const cell = worksheet2.getCell('C3');

        //* Modify/Add individual cell
        cell.value = new Date(1968, 2, 1);

        //* query a cell's type
        //*TODO: Code khong hoat dong
        // expect(cell.type).toEqual(ExcelJS.ValueType.Date);

        //* use string value of cell
        let myInput = 'Test ExcelJS'
        myInput = cell.text;
        // console.log(myInput)

        //* use html-safe string for rendering...
        //*TODO: Chua hieu chuc nang html-safe trong excel
        const html = '<div>' + cell.text + '</div>';
        // console.log(html)

        //*Merged Cells ==============================================================================================
        //* merge a range of cells
        // worksheet2.mergeCells('A4:B5');

        //* ... merged cells are linked
        //*TODO: Code khong hoat dong
        // worksheet2.getCell('B5').value = 'Hello, World!';
        // expect(worksheet2.getCell('B5').value).toBe(worksheet.getCell('A4').value);
        // expect(worksheet2.getCell('B5').master).toBe(worksheet.getCell('A4'));

        //* ... merged cells share the same style object
        //*TODO: Code khong hoat dong
        // expect(worksheet.getCell('B5').style).toBe(worksheet.getCell('A4').style);
        // worksheet.getCell('B5').style.font = myFonts.arial;
        // expect(worksheet.getCell('A4').style.font).toBe(myFonts.arial);

        //* unmerging the cells breaks the style links
        //*TODO: Code khong hoat dong
        // worksheet2.unMergeCells('A4');
        // expect(worksheet2.getCell('B5').style).not.toBe(worksheet.getCell('A4').style);
        // expect(worksheet2.getCell('B5').style.font).not.toBe(myFonts.arial);

        //* merge by top-left, bottom-right
        worksheet2.mergeCells('K10', 'M12');

        //* merge by start row, start column, end row, end column (equivalent to K10:M12)
        worksheet2.mergeCells(13,14,14,14);

        //*Insert Rows ===============================================================================================

        //*NOTE:
        //*TODO: insertRow(pos, value, styleOption = 'n')
        //* pos: Row number where you want to insert, pushing down all rows from there
        //* value/s: the new row/s values
        //* styleOption: 'i' for inherit from row above, 'o' for original style, 'n' for none.

        //* Insert a couple of Rows by key-value, shifting down rows every time
        worksheet.insertRow(9, {id: 0, name: 'Marry', dob: new Date(1970,1,1)},'n');
        worksheet.insertRow(1, {id: 0, name: 'Joe', dob: new Date(1965,1,7)},'n');

        //* Insert a row by contiguous Array (assign to columns A, B & C)
        worksheet.insertRow(1, [0, 'Cathlyn', new Date()],'n');

        //* Insert a row by sparse Array (assign to columns A, E & I)
        //*TODO: Code khong hoat dong
        var rowValues1 = [];
        rowValues[1] = 0;
        rowValues[5] = 'Kyle';
        rowValues[9] = new Date();
        worksheet.insertRow(1, rowValues1,'n');

        //* Insert an array of rows, in position 1, shifting down current position 1 and later rows by 2 rows
        var rows1 = [
            [0,'Bob',new Date()], // row by array
            {id:0, name: 'Barbara', dob: new Date()}
        ];
        worksheet.insertRows(1, rows1,'i');

        //*Splice ====================================================================================================

        //* Cut one or more rows (rows below are shifted up)
        //* Known Issue: If a splice causes any merged cells to move, the results may be unpredictable
        // worksheet2.spliceRows(1, 4);

        //* remove one row and insert two more.
        //* Note: rows 4 and below will be shifted down by 1 row.
        // const newRow3Values = [1, 2, 3, 4, 5];
        // const newRow4Values = ['one', 'two', 'three', 'four', 'five'];
        // worksheet2.spliceRows(3, 1, newRow3Values, newRow4Values);

        //*Cut one or more cells (cells to the right are shifted left)
        //* Note: this operation will not affect other rows
        // const row4 = worksheet2.getRow(3)
        // row4.splice(1, 2);

        //* remove one cell and insert two more (cells to the right of the cut cell will be shifted right)
        // row4.splice(2, 1, 'new value 1', 'new value 2');

        //* Duplicate a Row ==========================================================================================
        //*TODO: duplicateRow(start, amount = 1, insert = true)
        //*NOTE:
        //* start: Row number you want to duplicate (first in excel is 1).
        //* amount: The times you want to duplicate the row. Default Value: 1
        //* insert: true if you want to insert new rows for the duplicates, or false if you want to replace them: Default Value: true

        const ws = workbook.addWorksheet('duplicateTest');
        ws.getCell('A1').value = 'One';
        ws.getCell('A2').value = 'Two';
        ws.getCell('A3').value = 'Three';
        ws.getCell('A4') .value = 'Four';

        //* This line will duplicate the row 'One' twice but it will replace rows 'Two' and 'Three'
        //* if third param was true so it would insert 2 new rows with the values and styles of row 'One'
        ws.duplicateRow(1,2,true);

        //*Data Validations =========================================================================================
        //*Cells can define what values are valid or not and provide prompting to the user to help guide them.

        //* Specify list of valid values (One, Two, Three, Four).
        //* Excel will provide a dropdown with these values.
        // worksheet2.getCell('A1').dataValidation = {
        //     type: 'list',
        //     allowBlank: true,
        //     formulae: ['"1,2,3,4"']
        // };

        //* Specify list of valid values from a range.
        //* Excel will provide a dropdown with these values.
        // worksheet2.getCell('A1').dataValidation = {
        //     type: 'list',
        //     allowBlank: true,
        //     formulae: ['$A$2:$C$3']
        // };

        //* Specify Cell must be a whole number that is not 5.
        //* Show the user an appropriate error message if they get it wrong
        // worksheet2.getCell('A2').dataValidation = {
        //     type: 'whole',
        //     operator: 'notEqual',
        //     showErrorMessage: true,
        //     formulae: [0],
        //     errorStyle: 'error',
        //     errorTitle: 'Zero',
        //     error: 'The value must not be Zero'
        // };

        //* Specify Cell must be a decimal number between 1.5 and 7.
        //* Add 'tooltip' to help guid the user
        // worksheet2.getCell('A3').dataValidation = {
        //     type: 'decimal',
        //     operator: 'between',
        //     allowBlank: true,
        //     showInputMessage: true,
        //     formulae: [1.5, 7],
        //     promptTitle: 'Decimal',
        //     prompt: 'The value must between 1.5 and 7'
        // };

        //* Specify Cell must be have a text length less than 15
        // worksheet2.getCell('A4').dataValidation = {
        //     type: 'textLength',
        //     operator: 'lessThan',
        //     showErrorMessage: true,
        //     allowBlank: true,
        //     formulae: [5]
        // };

        //* Specify Cell must be have be a date before 1st Jan 2016
        // worksheet2.getCell('C4').dataValidation = {
        //     type: 'date',
        //     operator: 'lessThan',
        //     showErrorMessage: true,
        //     allowBlank: true,
        //     formulae: [new Date(2016,0,1)]
        // };

        //*Cell Comments =============================================================================================

        //* plain text note
        worksheet2.getCell('A1').note = 'Hello, ExcelJS!';

        //* colorful formatted note
        worksheet2.getCell('B1').note = {
            texts: [
                {'font': {'size': 12, 'color': {'theme': 1}, 'name': 'Calibri', 'family': 2, 'scheme': 'minor'}, 'text': 'This is '},
                {'font': {'italic': true, 'size': 12, 'color': {'theme': 1}, 'name': 'Calibri', 'scheme': 'minor'}, 'text': 'a'},
                {'font': {'size': 12, 'color': {'theme': 1}, 'name': 'Calibri', 'family': 2, 'scheme': 'minor'}, 'text': ' '},
                {'font': {'size': 12, 'color': {'argb': 'FFFF6600'}, 'name': 'Calibri', 'scheme': 'minor'}, 'text': 'colorful'},
                {'font': {'size': 12, 'color': {'theme': 1}, 'name': 'Calibri', 'family': 2, 'scheme': 'minor'}, 'text': ' text '},
                {'font': {'size': 12, 'color': {'argb': 'FFCCFFCC'}, 'name': 'Calibri', 'scheme': 'minor'}, 'text': 'with'},
                {'font': {'size': 12, 'color': {'theme': 1}, 'name': 'Calibri', 'family': 2, 'scheme': 'minor'}, 'text': ' in-cell '},
                {'font': {'bold': true, 'size': 12, 'color': {'theme': 1}, 'name': 'Calibri', 'family': 2, 'scheme': 'minor'}, 'text': 'format'},
            ],
            margins: {
                insetmode: 'custom',
                inset: [0.5, 0.5, 0.5, 0.5]
            },
            protection: {
                locked: "True",
                lockText: "False"
            },
            editAs: 'twoCells',
        };
        //*Table ====================================================================================================
        // add a table to a sheet
        // worksheet2.addTable({
        //     name: 'MyTable',
        //     ref: 'A11',
        //     headerRow: true,
        //     totalsRow: true,
            // style: {
            // theme: 'TableStyleDark3',
            // showRowStripes: true,
            // },
        //     columns: [
        //     {name: 'Date', totalsRowLabel: 'Totals:', filterButton: true},
        //     {name: 'Amount', totalsRowFunction: 'sum', filterButton: false},
        //     ],
        //     rows: [
        //     [new Date('2019-07-20'), 70.10],
        //     [new Date('2019-07-21'), 70.60],
        //     [new Date('2019-07-22'), 70.10],
        //     ],
        // });
        //*Modifying Table
        //*Adding or removing header and totals
        // const table = worksheet2.getTable('MyTable');

        //* turn header row on
        // table.headerRow = true;

        //* turn totals row off
        // table.totalsRow = false;

        //*Relocating a Table
        //* table top-left move to D4
        // table.ref = 'H4';

        //*Adding and Removing Rows
        //* remove first two rows
        // table.removeRows(0, 2);

        //* insert new rows at index 5
        // table.addRow([new Date('2019-08-05'), 5], 1);

        //* append new row to bottom of table
        // table.addRow([new Date('2019-08-10'), 10],1);

        //*Adding and Removing Columns
        //* remove second column
        // table.removeColumns(1, 1);

        //* insert new column (with data) at index 1
        // table.addColumn(
        //     {name: 'Letter', totalsRowFunction: 'custom', totalsRowFormula: 'ROW()', filterButton: true},
        //     ['a', 'b', 'c'],2
        // );
        
        //* Change Columns Properties
        //* Get Column Wrapper for second column
        // const column = table.getColumn(1);

        //* set some properties
        // column.name = 'Code';
        // column.filterButton = true;

        //*TODO: dong code loi
        // column.style = {font:{bold: true, name: 'Comic Sans MS'}};

        // column.totalsRowLabel = 'Totals';
        // column.totalsRowFunction = 'sum';
        // column.totalsRowFormula = 'ROW()';

        //*TODO: dong code loi
        // column.totalsRowResult = 10;

        //* commit the table changes into the sheet
        // table.commit();

        //*Value Types ====================================================================================================

        //*Null Value
        //*Note: A null value indicates an absence of value and will typically not be stored when written to file (except for merged cells).
        //*      It can be used to remove the value from a cell.
        // worksheet2.getCell('A5').value = null;

        //*Merge Cell
        //*Note: A merge cell is one that has its value bound to another 'master' cell. Assigning to a merge cell will cause the master's cell to be modified.
        // worksheet2.mergeCells('A6:A7')

        //*Number Value =====================
        // worksheet2.getCell('A9').value = 99;
        // worksheet2.getCell('A10').value = 99.999;

        //*String Value =====================
        // worksheet2.getCell('B9').value = 'Hello, World!';

        //*Date Value =======================
        // worksheet2.getCell('B9').value = new Date();

        //*Hyperlink Value ==================
        //* link to web
        // worksheet2.getCell('B9').value = {
        //     text: 'www.google.com',
        //     hyperlink: 'http://www.google.com',
        //     //*TODO: Code loi
        //     // tooltip: 'www.mylink.com'
        // };
        //* internal link
        // worksheet2.getCell('B10').value = { text: 'Sheet2', hyperlink: '#\'Sheet2\'!A1' };

        //*Rich Text Value
        // worksheet2.getCell('A9').value = {
        //     richText: [
        //       { text: 'This is '},
        //       {font: {italic: true}, text: 'italic'},
        //     ]
        //   };

        //*Boolean Value======================
        // worksheet2.getCell('A9').value = true;
        // worksheet2.getCell('A10').value = false;

        //*Error Value========================
        //*TODO: Chưa rõ chức năng 
        // worksheet.getCell('A9').value = { error: '#N/A' };
        // worksheet.getCell('A10').value = { error: '#VALUE!' };

        //* write and read file ======================================================================================
        // console.log(worksheet2.getRow(2).values)

        await workbook.xlsx.writeFile('export.xlsx');
        const readFile = await workbook.xlsx.readFile('export.xlsx');

        // const idCol1 = readFile.worksheets[0].getColumnKey('id').values;

        await readFile.xlsx.writeFile('export1.xlsx')
        const getWorkSheet1 = readFile.getWorksheet('My Sheet');
        // console.log(numValues)


    }
}
