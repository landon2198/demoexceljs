import { Module } from '@nestjs/common';
import { HandleTemplateController } from './handle-template.controller';
import { HandleTemplateService } from './handle-template.service'

@Module({
  providers: [HandleTemplateService],
  controllers: [HandleTemplateController]
})
export class HandleTemplateModule {}
