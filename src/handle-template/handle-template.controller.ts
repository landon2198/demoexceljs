import { Controller, Post } from '@nestjs/common';
import { HandleTemplateService } from './handle-template.service'

@Controller('handle-template')
export class HandleTemplateController {
    constructor(
        private handleTemplateService : HandleTemplateService
    ){}

    @Post('/')
    handleTemplateExc(){
        return this.handleTemplateService.handleFileExc();
    }
}
