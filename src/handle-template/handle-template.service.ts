import { Injectable } from '@nestjs/common';
import * as ExcelJS from 'exceljs';
import { async } from 'rxjs';
import * as XLSX from 'xlsx';

@Injectable()
export class HandleTemplateService {
    constructor() {
    }

    async handleFileExc(){
        const workbook = new ExcelJS.Workbook();
        await workbook.xlsx.readFile('template.xlsx')
        .then(async function(){
            return await workbook.xlsx.writeFile('teamplateCopy.xlsx')
        });
    }
}
