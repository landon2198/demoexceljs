import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('excel')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/create')
  getHello(): string {
    return this.appService.getHello();
  }
}
